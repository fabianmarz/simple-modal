#Simple Modal with Markdown Language Support
First of all you have to be sure, **jQuery 1.4.3+** is initialized

Add the the Source file right before the closing tag of the body

	<script src="path/to/your/source/jquery.simplemodal.js"></script>
	
Create a link, button, whatever with a class or id of your choice e.g.

	<a href="#" class="modal">Launch modal</a>
	
To pass an **external content** into your modal, simply write:
	
	<a href="#" class="modal" data-load-div="#modalID">
	
	OR
	
	<a href="#" class="modal" data-load-div=".modalClass">

To pass **inline content** simply write:

	<a href="#" class="modal" data-content="**This is content!**">Modal with Inline Content</a>


The content, which you can write in [Markdown Language](http://daringfireball.net/projects/markdown/basics) will be pushed to your Simple Modal!

**Simply** add **Markdown** Syntax to your div to generate an **awesome** looking modal!

To initialize **Simple Modal**, just write this codeblock into the DOM Ready function

	$('.modal').simpleModal();

You can also pass width - default is **400px** - and maximum width - default is **600px**. Just type it like a css selector
	
	$('.modal').simpleModal({
		width:    '80%'
		maxWidth: '500px'
	});
	
	both accept all known CSS measurement units
	

Further todos will be:

- add more options
- iframe support
- create / push buttons w/o function support 

	
	